import React,{useEffect, useState} from 'react';
import Person from './components/Person';
import axios from 'axios';
import './App.css';

function App() {

  const [data,setData] = useState([]);
  const [search,setSearch] = useState('');

  
    useEffect( () => {

        const getData = async () => {

            let getData = await axios.get('https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json')

            getData = getData.data;

            getData.sort((a, b) => {

              if (a.last_name.toLowerCase() < b.last_name.toLowerCase()) return -1;
              if (a.last_name.toLowerCase() > b.last_name.toLowerCase()) return 1;
              return 0;

            })

            setData(getData)

        }

        getData()

    }, [] )

    const handleSearch = e => {
      setSearch(e.target.value);
    }


  return (
    <>

    <div className="contact"><p>Contacts</p></div>

    <div className="searchDiv">
      <i className="fas fa-search"></i> <input type="text" className="search" onChange={handleSearch} value={search}/>
    </div>
    <ul>

      {data.filter( el => {
        if (search === "") {
          return el
        } else if (el.first_name.toLowerCase().includes(search.toLowerCase()) || el.last_name.toLowerCase().includes(search.toLowerCase())){
          return el
        }
      }).map(el => (
        <Person key={el.id} avatar={el.avatar} firstName={el.first_name} lastName={el.last_name} email={el.email} id={el.id}/>
      ))}

    </ul>

    </>
  );
}

export default App;
