import React,{useRef,useState} from 'react';
import '../person.css';

const Person = ({ avatar, firstName, lastName, email, id }) => {

    const [color,setColor] = useState(false);

    const idRef = useRef();

    const handleFocus = () => {

        console.log(idRef.current.id);
        setColor(!color);

    }

    return ( 
        <>
        <li className={color ? 'container showColor': 'container'} onClick={handleFocus} id={id} ref={idRef}>
            <img src={avatar} alt="" className="avatar"/>
            <div className="box">
                <h3 className="name">{firstName} {lastName}</h3>
                <span className="email">{email}</span>
            </div>
        </li>
        </>
     );
}
 
export default Person;